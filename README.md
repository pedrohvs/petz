# Petz

API REST utilizando SPRINGBOOT

# Objetivo
Disponibilização de uma API para integração com outros sistemas para as entidades CLIENTE e PET.

Demonstração na Controller TEST e AUTH do funcionamento com as rotas configuradas do SPRINGBOOT DATA SECURITY.

Documentação da API utilizando SWAGGER. http://localhost:8080/swagger-ui.html

Criação de um teste unitário simples utilizando JUNIT

Dockerfile na raiz do projeto caso queira utilizar com container.

Existe um arquivo chamado Schema.sql dentro de Resources para subir a estrutura do banco de dados.

Os application-dsv.properties com usuário e senha do ambiente de DEV não subiu devido à restrição criada no .gitignore

# Banco de dados utilizado
PostgreSQL 

# Bibliotecas utilizadas
LOMBOK - Para deixar o código mais limpo e evitar criação de métodos na MODEL

SWAGGER - Para documentação da api. 

JUNIT 5 - Para criação de teste unitário

SPRINGBOOT WEB - Para criação de API RESTFUL

SPRINGBOOT DATA SECURITY - Para demonstraçao da utilização de segurança utilizando JWT e criação das segurança das rotas

SPRINGBOOT VALIDATION - Para validação de dados

SPRINGBOOT DEVTOOLS - Para facilitar na hora do desenvolvimento

JJWT - Para manipulação do JWT

JPA/HIBERNATE - Para persistência de dados




