FROM openjdk:14-alpine

COPY target/*.jar /api.jar

WORKDIR /

EXPOSE 8080

CMD java -jar api.jar
