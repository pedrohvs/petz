package br.com.petz.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.petz.Controller.ClienteController;
import br.com.petz.Model.Cliente;

@SpringBootTest
@AutoConfigureMockMvc
public class ClienteValidatorRestApplicationTests {

	@Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ClienteController controller;
    
    
    @Test
    void case1() throws Exception {
    	
    	Cliente cliente = new Cliente((long) 1,"nome1","emailteste@email.com","61999991234");
    	mockMvc.perform(post("/api/clientes")
    	        .contentType(MediaType.APPLICATION_JSON)
    	        .content(objectMapper.writeValueAsString(cliente)))
    	        .andExpect(status().isOk());
    	
    	Cliente clienteRetorno = controller.findByEmail(cliente.getEmail()).getBody();

    	mockMvc.perform(get("/api/clientes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(cliente)))
                .andExpect(status().isOk());
        // JUnit5
        Assertions.assertEquals(clienteRetorno.getNome(),"nome1");
        Assertions.assertEquals(clienteRetorno.getEmail(), "emailteste@email.com");
        Assertions.assertEquals(clienteRetorno.getTelefone(),"61999991234");
    }
}
