-- DROP DATABASE petz;

CREATE DATABASE "petz"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

--Sequences

-- SEQUENCE: public.cliente_id_seq

-- DROP SEQUENCE public.cliente_id_seq;

CREATE SEQUENCE public.cliente_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.cliente_id_seq
    OWNER TO postgres;
	
-- SEQUENCE: public.pet_id_seq

-- DROP SEQUENCE public.pet_id_seq;

CREATE SEQUENCE public.pet_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.pet_id_seq
    OWNER TO postgres;
	
-- SEQUENCE: public.roles_id_seq

-- DROP SEQUENCE public.roles_id_seq;

CREATE SEQUENCE public.roles_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.roles_id_seq
    OWNER TO postgres;
	
-- SEQUENCE: public.users_id_seq

-- DROP SEQUENCE public.users_id_seq;

CREATE SEQUENCE public.users_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.users_id_seq
    OWNER TO postgres;


-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    id bigint NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    email character varying(50) NOT NULL COLLATE pg_catalog."default",
    password character varying(120) NOT NULL COLLATE pg_catalog."default",
    username character varying(20) NOT NULL COLLATE pg_catalog."default",
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT ukUsers_email UNIQUE (email),
    CONSTRAINT ukUsers_username UNIQUE (username)
)

TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;

-- Table: public.roles

-- DROP TABLE public.roles;

CREATE TABLE public.roles
(
    id integer NOT NULL DEFAULT nextval('roles_id_seq'::regclass),
    name character varying(20) NOT NULL COLLATE pg_catalog."default",
    CONSTRAINT roles_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.roles
    OWNER to postgres;
	
-- Table: public.user_roles

-- DROP TABLE public.user_roles;

CREATE TABLE public.user_roles
(
    user_id bigint NOT NULL,
    role_id integer NOT NULL,
    CONSTRAINT user_roles_pkey PRIMARY KEY (user_id, role_id),
    CONSTRAINT fkUserRoles_role FOREIGN KEY (role_id)
        REFERENCES public.roles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkUserRoles_user FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.user_roles
    OWNER to postgres;


-- Table: public.cliente

-- DROP TABLE public.cliente;

CREATE TABLE public.cliente
(
    id bigint NOT NULL DEFAULT nextval('cliente_id_seq'::regclass),
    email character varying(50) NOT NULL COLLATE pg_catalog."default",
    nome character varying(50) NOT NULL COLLATE pg_catalog."default",
    telefone character varying(11) COLLATE pg_catalog."default",
    CONSTRAINT cliente_pkey PRIMARY KEY (id),
	CONSTRAINT ukUsers_email UNIQUE (email)
)

TABLESPACE pg_default;

ALTER TABLE public.cliente
    OWNER to postgres;

-- Table: public.pet

-- DROP TABLE public.pet;

CREATE TABLE public.pet
(
    id bigint NOT NULL DEFAULT nextval('pet_id_seq'::regclass),
    data_nascimento date,
    nome character varying(50) NOT NULL COLLATE pg_catalog."default",
    tipo_animal character varying(50) NOT NULL COLLATE pg_catalog."default",
	raca character varying(50) NOT NULL COLLATE pg_catalog."default",
    cliente_id bigint,
    CONSTRAINT pet_pkey PRIMARY KEY (id),
    CONSTRAINT fkPet_Cliente FOREIGN KEY (cliente_id)
        REFERENCES public.cliente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.pet
    OWNER to postgres;
	
	

insert into roles(id,name) values(1,'ROLE_USER');
insert into roles(id,name) values(2,'ROLE_MODERATOR');
insert into roles(id,name) values(3,'ROLE_ADMIN');