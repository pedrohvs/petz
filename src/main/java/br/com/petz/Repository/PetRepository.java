package br.com.petz.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.petz.Model.Cliente;
import br.com.petz.Model.Pet;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long> { }