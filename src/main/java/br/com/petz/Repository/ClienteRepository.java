package br.com.petz.Repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.petz.Model.Cliente;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> { 
	Optional<Cliente> findByEmail(String email);
}
