package br.com.petz.Controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.petz.Model.Pet;
import br.com.petz.Repository.PetRepository;

@RestController
@RequestMapping({"/api/pets"})
public class PetController {

  private PetRepository repository;

  PetController(PetRepository petRepository) {
      this.repository = petRepository;
  }

  // CRUD methods here
  
  @GetMapping
  public List<Pet> findAll(){
    return repository.findAll();
  }
  
  @GetMapping(path = {"/{id}"})
  public ResponseEntity<Pet> findById(@PathVariable long id){
    return repository.findById(id)
            .map(record -> ResponseEntity.ok().body(record))
            .orElse(ResponseEntity.notFound().build());
  }
  
  @PostMapping
  public Pet create(@RequestBody Pet Pet){
      return repository.save(Pet);
  }
  
  @PutMapping(value="/{id}")
  public ResponseEntity<Pet> update(@PathVariable("id") long id,
                                        @RequestBody Pet pet){
    return repository.findById(id)
        .map(record -> {
            record.setNome(pet.getNome());
            record.setDataNascimento(pet.getDataNascimento());
            record.setRaca(pet.getRaca());
            record.setTipoAnimal(pet.getTipoAnimal());
            record.setCliente(pet.getCliente());
            Pet updated = repository.save(record);
            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
  }
  
  @DeleteMapping(path ={"/{id}"})
  public ResponseEntity<?> delete(@PathVariable("id") long id) {
    return repository.findById(id)
        .map(record -> {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }).orElse(ResponseEntity.notFound().build());
  }
}